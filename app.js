/*-----------------------------------------------------------------------------
A simple Language Understanding (LUIS) bot for the Microsoft Bot Framework.
-----------------------------------------------------------------------------*/

var restify = require('restify');
var builder = require('botbuilder');
var botbuilder_azure = require("botbuilder-azure");
var request = require('request');
var category = 'np_category';
let page = 0;

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 4040, function () {
    console.log('%s listening to %s', server.name, server.url);
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata
});

// Listen for messages from users
server.post('/api/messages', connector.listen());

/*----------------------------------------------------------------------------------------
* Bot Storage: This is a great spot to register the private state storage for your bot.
* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
* ---------------------------------------------------------------------------------------- */

var tableName = 'botdata';
var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, process.env['AzureWebJobsStorage']);
var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);

// Create your bot with a function to receive messages from the user
// This default message handler is invoked if the user's utterance doesn't
// match any intents handled by other dialogs.
var bot = new builder.UniversalBot(connector, function (session, args) {
    session.send('Sorry ! I didn\'t get you, please be more specific');
    session.endDialog();
});

bot.set('storage', tableStorage);

// Make sure you add code to validate these fields
var luisAppId = process.env.LuisAppId;
var luisAPIKey = process.env.LuisAPIKey;
var luisAPIHostName = process.env.LuisAPIHostName || 'westus.api.cognitive.microsoft.com';

const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v2.0/apps/' + luisAppId + '?subscription-key=' + luisAPIKey;

// Create a recognizer that gets intents from LUIS, and add it to the bot
var recognizer = new builder.LuisRecognizer(LuisModelUrl);
bot.recognizer(recognizer);

// Add a dialog for each intent that the LUIS app recognizes.
// See https://docs.microsoft.com/bot-framework/nodejs/bot-builder-nodejs-recognize-intent-luis
bot.dialog('GreetingDialog',
    (session) => {
        session.send('Hi!\n' +
            'Please tell me which corporate news you are interested in, and want me to search for?');
        session.endDialog();
    }
).triggerAction({
    matches: 'Greeting'
});

bot.dialog('HelpDialog',
    (session) => {
        session.send('Hi there! '+"\n"+' What news do I search for you Today?');
        session.endDialog();
    }
).triggerAction({
    matches: 'Help'
});

bot.dialog('CancelDialog',
    (session) => {
        session.send('Say "Talk to Corporate Newswire" to call me again.'+"\n"+'bye');
        session.endConversation();
        session.clearDialogStack();
    }
).triggerAction({
    matches: 'Cancel'
});


// CreateNote dialog
bot.dialog('CategoryDialog', [
    function (session, args, next) {
        var intent = args.intent;
        var input = builder.EntityRecognizer.findEntity(intent.entities, 'category');
        page =0;

        if(input.type == 'category' && typeof input.entity != 'undefined'){
            category = input.entity;
            let json = '{"originalRequest":{"source":"microsoft","data":{"category":"'+category+'","page":'+page+'}}}';

            request('https://90535897.ngrok.io/disclosure-newswire/CorporateNewsGoogle/?request='+json, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var data = JSON.parse(body);
                    page = data.page; // updating page number
                    category = data.category; // set the category returning from response

                    session.send('%s', data.response);
                }else{
                    session.send('Sorry! I didn\'t get what you just said. please choose more specific keywords');
                }
            })
        }



        //session.send('\'%s\'',title);
    },
]).triggerAction({
    matches: 'Category'
});

bot.dialog('MoreNewsDialog',
    (session) => {

        if(typeof category !='undefined' && category!='np_category'){

            let json = '{"originalRequest":{"source":"microsoft","data":{"category":"'+category+'","page":'+page+'}}}';
            request('https://90535897.ngrok.io/disclosure-newswire/CorporateNewsGoogle/?request='+json, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var data = JSON.parse(body);
                    page = data.page; // updating page number
                    category = data.category; // set the category returning from response

                    session.send('%s', data.response);
                    session.endDialog();
                }else{
                    session.send('You entity is  \'%s\'.', JSON.stringify(title));
                    session.endDialog();
                }
            })

        }else{
            session.send('Sorry! something goes wrong, We need to start over');
            session.endDialog();
        }

    }
).triggerAction({
    matches: 'More News'
});

bot.dialog('NewsActionDialog',
    (session) => {

        session.send('OK!'+"\n"+'Say "Talk to Corporate Newswire" to call me again.');
        session.endDialog();

    }
).triggerAction({
    matches: 'NewsAction'
});

bot.dialog('RePropmptNewsDialog',
    (session) => {

        session.send('Alright!'+"\n"+'Please tell me which other corporate news you are interested in, and want me to search for?');
        session.endDialog();

    }
).triggerAction({
    matches: 'RePropmptNews'
});